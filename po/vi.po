# Vietnamese translation for Gimp Ruby.
# Copyright © 2008 GNOME i18n Project for Vietnamese.
# Clytie Siddall <clytie@riverland.net.au>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: gimp-ruby GNOME TRUNK\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-06-06 03:46+0000\n"
"PO-Revision-Date: 2008-06-06 17:52+0930\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LocFactoryEditor 1.7b3\n"

#: ../ext/ruby-fu-console.c:127
msgid "Ruby-Fu Console"
msgstr "Bàn giao tiếp Ruby-Fu"

#: ../plug-ins/irbconsole.rb:25
msgid "Starts an irb session in a console."
msgstr "Khởi chạy một buổi hợp IRB trên bàn giao tiếp."

#: ../plug-ins/irbconsole.rb:30
msgid "Irb Console"
msgstr "Bàn giao tiếp IRB"

#: ../plug-ins/irbconsole.rb:43
msgid "Interactive Gimp-Ruby Console"
msgstr "Bàn giao tiếp Ruby-GIMP tương tác"

#: ../plug-ins/irbconsole.rb:44
msgid "Ruby version #{RUBY_VERSION}"
msgstr "Phiên bản Ruby #{RUBY_VERSION}"

#: ../plug-ins/sphere.rb:33
#: ../plug-ins/sphere.rb:34
msgid "Simple sphere with a drop shadow"
msgstr "Hình cầu đơn giản có bóng thả"

#: ../plug-ins/sphere.rb:38
msgid "Sphere"
msgstr "Cầu"

#: ../plug-ins/sphere.rb:41
msgid "Radius (pixels)"
msgstr "Bán kính (điểm ảnh)"

#: ../plug-ins/sphere.rb:42
msgid "Lighting (degrees)"
msgstr "Nguồn sáng (độ)"

#: ../plug-ins/sphere.rb:43
msgid "Shadow"
msgstr "Bóng"

#: ../plug-ins/sphere.rb:44
msgid "Background Color"
msgstr "Màu nền"

#: ../plug-ins/sphere.rb:45
msgid "Sphere Color"
msgstr "Màu cầu"

#: ../plug-ins/sphere.rb:47
msgid "Sphere Image"
msgstr "Ảnh cầu"

#: ../plug-ins/sphere.rb:68
msgid "Sphere Layer"
msgstr "Lớp cầu"
